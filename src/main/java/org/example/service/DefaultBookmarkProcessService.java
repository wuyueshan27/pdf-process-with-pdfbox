package org.example.service;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.example.converter.DefaultConverter;
import org.example.pojo.Bookmark;
import org.example.pojo.TitleAndPageNumber;
import org.example.service.base.AbstractBookmarkProcessTemplate;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 书签写入入口方法，实现了父类方法doGenerateBookmarkList
 */

@Slf4j
public class DefaultBookmarkProcessService extends AbstractBookmarkProcessTemplate {
    /**
     * 需要实现生成书签的方法，流程已经在父类定义，每一步的逻辑都可以自己定义
     * @Param txtFile 书签的文本文件
     * @Param beforeConvert 原始字符串预处理，如遇到空白行则会返回Optional.empty() 所以可以不用删除空行
     * @Param convertTitleAndPageNumber 分割字符串为title和pageNumber 如果不能分割为标题和页码则会返回Optional.empty() 此行不会生成Bookmark
     * @Param convert2Bookmark 将title和pageNumber 按照等级规则 生成ArrayList<Bookmark>
     *
     */
    @Override
    public ArrayList<Bookmark> doGenerateBookmarkList(String txtFile) {
        return fromTxtGenerateBookmarkList(txtFile, DefaultConverter.defaultBeforeConvert, defaultConvertTitleAndPageNumber, DefaultConverter.defaultConvert2Bookmark);
    }

    public final static Function<Optional<String>, Optional<TitleAndPageNumber>> defaultConvertTitleAndPageNumber = (line) -> {
        if (line.isPresent()) {
            String reversedLine = StrUtil.reverse(line.get());
            Pattern pattern = Pattern.compile("(^\\d{1,4})(.*)"); // 切分为两部分，数字和字符串
            Matcher matcher = pattern.matcher(reversedLine);
            while (matcher.find()) {
                String reversedPageNumberString = matcher.group(1);
                String reversedTitleString = matcher.group(2);
                TitleAndPageNumber titleAndPageNumber = new TitleAndPageNumber(StrUtil.reverse(StrUtil.trim(reversedTitleString)), Convert.toInt(StrUtil.reverse(StrUtil.trim(reversedPageNumberString))));
                return Optional.of(titleAndPageNumber);
            }
            log.error("错误行==================未匹配到title和页码");
        }
        // 传入不存在或者为emtpy则返回empty
        return Optional.empty();
    };


}
