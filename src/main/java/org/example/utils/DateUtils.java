package org.example.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {
    public static String getDataStr() {
        LocalDateTime now = LocalDateTime.now();
        // 创建一个格式化器，定义所需的日期时间格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm");
        // 使用格式化器格式化LocalDateTime对象
        String formattedDateTime = now.format(formatter);
        return formattedDateTime;
    }
}
