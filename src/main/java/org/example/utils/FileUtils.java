package org.example.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileUtils {
    public static void outputContents(File path){
        //从文件中读取每一行的UTF-8编码数据
        ArrayList<String> readUtf8Lines = FileUtil.readLines(path, CharsetUtil.UTF_8, new ArrayList<>());
        for(String readUtf8Line :readUtf8Lines){
            System.out.println(readUtf8Line);
        }
    }

    public static void checkFileExists(String file){
        Path filePath = Paths.get(file);

        // 使用assert来断言文件存在
        assert Files.exists(filePath) : "文件不存在: " + filePath;
    }

    public static void main(String[] args) {
        checkFileExists("");
    }
}
