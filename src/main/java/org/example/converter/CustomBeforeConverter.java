package org.example.converter;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.function.Function;

@Slf4j
public class CustomBeforeConverter implements Function<String, Optional<String>> {
    @Override
    public Optional<String> apply(String line) {
        if (StrUtil.trim(line).length() > 0) {
            return Optional.of(StrUtil.trim(line));
        }
        log.error("===============空行错误===============");
        return Optional.empty();
    }

}
