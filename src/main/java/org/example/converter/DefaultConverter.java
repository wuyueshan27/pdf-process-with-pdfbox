package org.example.converter;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.example.pojo.Bookmark;
import org.example.pojo.TitleAndPageNumber;

import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class DefaultConverter {
    public final static Function<String, Optional<String>> defaultBeforeConvert = (line) -> {
        if (StrUtil.trim(line).length() > 0) {
            return Optional.of(StrUtil.trim(line));
        }
        log.error("===============空行错误===============");
        return Optional.empty();
    };
    public final static Function<Optional<String>, Optional<TitleAndPageNumber>> defaultConvertTitleAndPageNumber = (line) -> {
        if (line.isPresent()) {
            String reversedLine = StrUtil.reverse(line.get());
            Pattern pattern = Pattern.compile("(^\\d{1,3})(.*)"); // 切分为两部分，数字和字符串
            Matcher matcher = pattern.matcher(reversedLine);
            while (matcher.find()) {
                String reversedPageNumberString = matcher.group(1);
                String reversedTitleString = matcher.group(2);
                TitleAndPageNumber titleAndPageNumber = new TitleAndPageNumber(StrUtil.reverse(StrUtil.trim(reversedTitleString)), Convert.toInt(StrUtil.reverse(StrUtil.trim(reversedPageNumberString))));
                return Optional.of(titleAndPageNumber);
            }
            log.error("错误行==================未匹配到title和页码");
        }
        // 传入不存在或者为emtpy则返回empty
        return Optional.empty();
    };
    public final static Function<Optional<TitleAndPageNumber>, Optional<Bookmark>> defaultConvert2Bookmark = titleAndPageNumber -> {
        if (titleAndPageNumber.isPresent()) {
            String title = titleAndPageNumber.get().getTitle();
            Integer pageNumber = titleAndPageNumber.get().getPageNumber();
            if (title.matches(".*第.*章.*")) {
                Bookmark bookmark = new Bookmark();
                bookmark.setTitle(title);
                bookmark.setPageNumber(pageNumber);
                bookmark.setLevel(0);
                return Optional.of(bookmark);
            }
            // 非章标题
            Bookmark bookmark = new Bookmark();
            bookmark.setTitle(title);
            bookmark.setPageNumber(pageNumber);
            bookmark.setLevel(getTitleLevel(title, '.'));
            return Optional.of(bookmark);

        }
        return Optional.empty();
    };

    public static int getTitleLevel(String title, char targetChar) {
        String regex = "([\\d\\.]+).*";
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(title);
        while (matcher.find()) {
            if (matcher.group(1).endsWith(".")) {
                return countCharacters(matcher.group(1).substring(0, matcher.group(1).length() - 1), targetChar);
            }
            return countCharacters(matcher.group(1), targetChar);
        }
        // 没有匹配到则都设置为level为0
        return 0;
    }

    private static int countCharacters(String str, char targetChar) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == targetChar) {
                count++;
            }
        }
        return count;
    }

}
