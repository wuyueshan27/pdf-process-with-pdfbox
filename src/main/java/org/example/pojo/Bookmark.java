package org.example.pojo;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Bookmark {
    private String title;
    private Integer pageNumber;
    private Integer level;
}
