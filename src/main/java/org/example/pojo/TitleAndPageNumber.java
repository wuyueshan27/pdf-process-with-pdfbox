package org.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TitleAndPageNumber {
    private final String title;
    private final Integer pageNumber;
}
