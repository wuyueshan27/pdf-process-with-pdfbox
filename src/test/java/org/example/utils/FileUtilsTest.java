package org.example.utils;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileUtilsTest {
    @Test
    public void test1() {
        // 获取当前工作目录的路径
        String currentDirectory = System.getProperty("user.dir");

        // 打印当前工作目录的路径
        System.out.println("Current Directory: " + currentDirectory);
    }


    @Test
    public void test6() {
        LocalDateTime now = LocalDateTime.now();
        // 创建一个格式化器，定义所需的日期时间格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm");
        // 使用格式化器格式化LocalDateTime对象
        String formattedDateTime = now.format(formatter);
        // 打印格式化后的日期时间字符串
        System.out.println("Formatted LocalDateTime: " + formattedDateTime);
    }

}
