package org.example.pdfbox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;

public class PDFBoxBookmarkExample {
    public static void main(String[] args) {
        // 创建一个新的PDF文档
        PDDocument document = new PDDocument();

        // 添加一个页面
        PDPage page1 = new PDPage();
        document.addPage(page1);
        // 添加一个页面
        PDPage page2 = new PDPage();
        document.addPage(page2);
        // 创建文档大纲
        PDDocumentOutline outline = new PDDocumentOutline();
        document.getDocumentCatalog().setDocumentOutline(outline);

        // 创建根书签即顶级书签指的是章
        PDOutlineItem rootBookmark = new PDOutlineItem();
        rootBookmark.setTitle("Root Bookmark");
        outline.addLast(rootBookmark);

        // 设置根书签跳转到的页面
        PDPage destPage = document.getPage(0); // 跳转到第一页
        PDOutlineItem dest = new PDOutlineItem();
        dest.setDestination(destPage);
        rootBookmark.setDestination(destPage);


        // 创建子书签小节
        PDOutlineItem childBookmark = new PDOutlineItem();
        childBookmark.setTitle("Child Bookmark");
        rootBookmark.addLast(childBookmark);

        // 设置子书签跳转到的页面
        PDPage destPage2 = document.getPage(1); // 跳转到第一页
        PDOutlineItem dest2 = new PDOutlineItem();
        dest.setDestination(destPage2);
        childBookmark.setDestination(destPage2);

        // 保存文档
        try {
            document.save("bookmark_example.pdf");
            System.out.println("PDF with bookmarks created successfully.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭文档
            try {
                document.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
