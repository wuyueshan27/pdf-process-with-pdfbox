package org.example;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

@SpringBootTest
@Slf4j
public class ApplicationContextTest {
    @Autowired
    ApplicationContext applicationContext;
    @Test
    public void test1(){
        System.out.println(applicationContext);
    }
    @Test
    public void test2(){
        String[] names = applicationContext.getBeanDefinitionNames();
        Arrays.stream(names)
                .forEach(name->log.info("bean name ==={}",name));
        log.info("application bean size = {}",names.length);
    }
}
