# 介绍
- 添加txt的文件书签到pdf文件
- 入口 org.example.service.DefaultBookmarkProcessServiceTest
- 从txt文件转换为书签的逻辑[DefaultBookmarkProcessService.java](src/main/java/org/example/service/DefaultBookmarkProcessService.java)中调用
- 添加书签逻辑在其父类中实现。
- 具体逻辑可以自己定义，可以写在类里面也可以自己实现接口，主要是如何生成书签的逻辑。